<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => "User 1",
                    'email' => str_random(5) . 'gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => 'Address Sample',
                    'phone' => '+7 777 707 07 07',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => "User 2",
                    'email' => str_random(5) . 'gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => 'Address Sample',
                    'phone' => '+7 777 707 07 07',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => "User 3",
                    'email' => str_random(5) . 'gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => 'Address Sample',
                    'phone' => '+7 777 707 07 07',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => "User 4",
                    'email' => str_random(5) . 'gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => 'Address Sample',
                    'phone' => '+7 777 707 07 07',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => "User 5",
                    'email' => str_random(5) . 'gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => 'Address Sample',
                    'phone' => '+7 777 707 07 07',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => "User 6",
                    'email' => str_random(5) . 'gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => 'Address Sample',
                    'phone' => '+7 777 707 07 07',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => "User 7",
                    'email' => str_random(5) . 'gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => 'Address Sample',
                    'phone' => '+7 777 707 07 07',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => "User 8",
                    'email' => str_random(5) . 'gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => 'Address Sample',
                    'phone' => '+7 777 707 07 07',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => "User 9",
                    'email' => str_random(5) . 'gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => 'Address Sample',
                    'phone' => '+7 777 707 07 07',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => "User 10",
                    'email' => str_random(5) . 'gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => 'Address Sample',
                    'phone' => '+7 777 707 07 07',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            ]
        );
    }
}
