<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question')->insert(
            [
                [
                'title' => 'Impossible question',
                'description' => 'How you did that?',
                'product_id' => 34
                ],
                [
                    'title' => 'Hard question',
                    'description' => 'How you did that?',
                    'product_id' => 34
                ],
                [
                    'title' => 'Complex question',
                    'description' => 'How you did that?',
                    'product_id' => 34
                ],
                [
                    'title' => 'Super complicated question',
                    'description' => 'How you did that?',
                    'product_id' => 34
                ],
                [
                    'title' => 'Supa dupa question',
                    'description' => 'How you did that?',
                    'product_id' => 34
                ],
                [
                'title' => 'Impossible question',
                'description' => 'How you did that?',
                'product_id' => 35
            ],
                [
                    'title' => 'Hard question',
                    'description' => 'How you did that?',
                    'product_id' => 35
                ],
                [
                    'title' => 'Complex question',
                    'description' => 'How you did that?',
                    'product_id' => 35
                ],
                [
                    'title' => 'Super complicated question',
                    'description' => 'How you did that?',
                    'product_id' => 35
                ],
                [
                    'title' => 'Supa dupa question',
                    'description' => 'How you did that?',
                    'product_id' => 35
                ],
                ]
        );
    }
}
