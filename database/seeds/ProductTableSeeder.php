<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product')->insert(
            [
                [
                    'name' => 'Product #1',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Suspendisse pulvinar nisl ut eleifend tempus',
                    'price' => 99.99,
                    'image' => 'img/image.png',
                    'subcat_id' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Product #2',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Suspendisse pulvinar nisl ut eleifend tempus',
                    'price' => 99.99,
                    'image' => 'img/image.png',
                    'subcat_id' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Product #3',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Suspendisse pulvinar nisl ut eleifend tempus',
                    'price' => 99.99,
                    'image' => 'img/image.png',
                    'subcat_id' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Product #4',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Suspendisse pulvinar nisl ut eleifend tempus',
                    'price' => 99.99,
                    'image' => 'img/image.png',
                    'subcat_id' => 2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Product #5',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Suspendisse pulvinar nisl ut eleifend tempus',
                    'price' => 99.99,
                    'image' => 'img/image.png',
                    'subcat_id' => 2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Product #6',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Suspendisse pulvinar nisl ut eleifend tempus',
                    'price' => 99.99,
                    'image' => 'img/image.png',
                    'subcat_id' => 2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Product #7',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Suspendisse pulvinar nisl ut eleifend tempus',
                    'price' => 99.99,
                    'image' => 'img/image.png',
                    'subcat_id' => 3,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Product #8',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Suspendisse pulvinar nisl ut eleifend tempus',
                    'price' => 99.99,
                    'image' => 'img/image.png',
                    'subcat_id' => 3,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Product #9',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Suspendisse pulvinar nisl ut eleifend tempus',
                    'price' => 99.99,
                    'image' => 'img/image.png',
                    'subcat_id' => 4,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Product #10',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Suspendisse pulvinar nisl ut eleifend tempus',
                    'price' => 99.99,
                    'image' => 'img/image.png',
                    'subcat_id' => 4,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]

            ]
        );
    }
}
