@extends('layout')
    @section('content')
        <div id="content">
            <div class="container" id="contact">

                <section>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="heading">
                                <h3>Загрузка товара</h3>
                            </div>

                            <form method="post" action="{{ route('product-store') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name">Наименование</label>
                                            <input name="name" type="text" class="form-control" id="name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="price">Цена</label>
                                            <input name="price" type="text" class="form-control" id="price">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="description">Описание</label>
                                            <textarea name="description" type="text" class="form-control" id="description" rows="5"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-center">
                                        <button type="submit" name="upload" class="btn btn-template-main"></i>Добавить
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>


                    </div>


                </section>

            </div>
            <!-- /#contact.container -->
        </div>
        <!-- /#content -->

    @endsection('content')