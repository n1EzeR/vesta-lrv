<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <title>Online Store</title>
</head>
<body>
<header>
    <!--TOP-->
    <div class="container-fluid bg-dark">
        <div class="container">
            <nav class="navbar navbar-light justify-content-between">
                <p class="">Online Store in Almaty. Contact us on +777 777 or hello@gmail.com.</p>
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Sign In</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Sign Up</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <!--NAVBAR-->
    <div class="container-fluid">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light" id="myNav">
                <a href="/" class="navbar-brand"><img src="{{ asset('img/logo.png') }}" alt="logo"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                        aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse justify-content-end navbar-collapse" id="navbarTogglerDemo01">
                    <ul class="navbar-nav">
                        <li class="nav-item active p-2" id="currentHome">
                            <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item p-2">
                            <a class="nav-link" href="/product/create">Product Add <span class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item p-2">
                            <a class="nav-link" href="/questions">All Questions <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown p-2">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                About Us
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Contacts</a>
                                <a class="dropdown-item" href="#">Info</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <!--NAME FILLE-->
    <div class="container-fluid bg-light">
        <div class="container filler">
            <h2 class="align-middle ">Vesta</h2>
        </div>
    </div>


</header>

@yield('content')

<footer>
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 offset-sm-1 footer-divs">
                    <h5>About Us</h5>
                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                    <div class="row footer-os">
                        <div class="col-sm-6"><img src="{{ asset('img/android.png') }}" alt="android"></div>
                        <div class="col-sm-6"><img src="{{ asset('img/apple.png') }}" alt="apple"></div>
                    </div>
                </div>
                <div class="col-sm-4 footer-divs"><h5>Contact</h5>
                    <p>Universal Ltd.
                        13/25 New Avenue
                        Newtown upon River
                        45Y 73J
                        England
                        Great Britain
                    </p>
                    <p>
                        <a href="#"><img class="social" src="{{ asset('img/vk.svg') }}" alt="vk"></a>
                        <a href="#"><img class="social" src="{{ asset('img/fb.svg') }}" alt="face"></a>
                        <a href="#"><img class="social" src="{{ asset('img/tw.svg') }}" alt="youtube"></a>
                        <a href="#"><img class="social" src="{{ asset('img/inst.svg') }}" alt="insta"></a>
                    </p>

                </div>
                <div class="col-sm-4 footer-divs"><h5>Right here</h5>
                    <a class="dg-widget-link"
                       href="http://2gis.kz/almaty/profiles/9429940000896618,9429940001098014,9429940001065164/center/76.98017120361328,43.27395582552914/zoom/12?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть
                        на карте Алматы</a>
                    <script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
                    <script charset="utf-8">new DGWidgetLoader({
                        "width": "100%",
                        "height": "auto",
                        "borderColor": "#a3a3a3",
                        "pos": {"lat": 43.27395582552914, "lon": 76.98017120361328, "zoom": 12},
                        "opt": {"city": "almaty"},
                        "org": [{"id": "9429940000896618"}, {"id": "9429940001098014"}, {"id": "9429940001065164"}]
                    });</script>
                    <noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript.
                        Включите его в настройках вашего браузера.
                    </noscript>
                </div>
            </div>
        </div>
    </div>
    <div id="copyright">
        <div class="container">
            <div class="col-md-10 text-center">
                <p class="pull-left">&copy; 2018. Company Name Inc.</p>

            </div>
        </div>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="{{ asset('js/sticky.js') }}"></script>
</body>
</html>
