@extends('layout')
@section('content')
    <main>
        <div class="container">
            <div class="row">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Question</th>
                        <th scope="col">Product ID</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($question as $q)
                    <tr>
                        <th scope="row">{{ $q -> id }}</th>
                        <td>{{ $q -> title }}</td>
                        <td>{{ $q -> description }}</td>
                        <td>{{ $q -> product_id }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $question->links() }}
            </div>
        </div>
    </main>
    @endsection('content')