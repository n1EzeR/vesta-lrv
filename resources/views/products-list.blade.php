@extends('layout')
@section('content')
    <main>
        <div class="container">
            <div class="row">
                <div class="col-3 mt-2">
                    <div class="panel panel-default sidebar-menu">
                        <div class="panel-heading">
                            <h3 class="panel-title">Categories</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Active
                                    </a>
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link " href="#">Active</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Link</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Link</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Link</a>
                                </li>
                            </ul>


                        </div>
                    </div>
                </div>
                <div class="col-9">
                    <p class="text-muted lead">
                        Sort by
                    <div class="btn-group" data-toggle="buttons">
                        <form id="sortForm" method="get" action="{{ route('sort') }}">
                            {{ csrf_field() }}
                        <ul class="list-inline">
                            <li class="list-inline-item" >
                                <a class="social-icon text-xs-center" href="#">
                                    <label>
                                        <input type="radio" {{ !empty($data['type']) and $type == "1" ? "checked" : "" }} class="btn btn-primary" name="asc" value="1" id="option1" autocomplete="off" onclick="document.getElementById('sortForm').submit();" > Price Ascending
                                    </label>

                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="social-icon text-xs-center" href="#" >
                                    <label>
                                        <input type="radio" {{ !empty($data['type']) and $type == "0" ? "checked" : "" }}  class="btn btn-primary" name="asc" value="0" id="option2" autocomplete="off"  onclick="document.getElementById('sortForm').submit();"> Price Descending
                                    </label>
                                </a>
                            </li>
                        </ul>

                        </form>
                    </div>

                    </p>
                    <div class="row">
                        @foreach($product as $p)
                        <div class="col-sm-4">
                            <div class="product">
                                <div class="image">
                                    <a href="/product/detail/{{ $p -> id }}">
                                        <img src="{{ $p -> image }}" class="img-fluid" alt="image">
                                    </a>
                                    <div class="text">
                                        <h3><a href="/product/detail/{{ $p -> id }}">{{ $p->name }}</a></h3>
                                        <p class="price">{{ $p->price }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                    <div class="row">
                        <div class="offset-sm-5 col-sm-5">{{ $product -> links() }}</div>
                    </div>

                </div>


            </div>
        </div>
        </div>
    </main>
    @endsection('content')