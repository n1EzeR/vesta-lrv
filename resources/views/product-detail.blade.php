@extends('layout')
@section('content')
    <div class="container">
        <div class="row">
            <p class="lead">
                <button class="btn"><a href="{{ route('product-read', $product->id) }}">Редактировать товар</a></button>
                <input type="button" value="Удалить товар" class="btn" id="del"></input>

            </p>
            <div class="col-9"><p class="text-muted lead">{{ $product -> description }}</p>
                <p class="goToDescription"><a href="#details" class="text-uppercase" onclick="">Scroll to product
                        details, material & care and sizing</a>
                </p>
                <div class="row" id="productMain">
                    <div class="col-sm-6">
                        <div id="mainImage">
                            <img src="{{ asset($product -> image) }}" alt="" class="img-fluid">
                        </div>

                    </div>

                    <div class="col-sm-6">
                        <div class="box">

                            <form>
                                <div class="sizes">

                                    <h3>{{ $product -> name }}</h3>

                                    <label for="size_s">
                                        <a href="#">S</a>
                                        <input type="radio" id="size_s" name="size" value="s" class="size-input">
                                    </label>
                                    <label for="size_m">
                                        <a href="#">M</a>
                                        <input type="radio" id="size_m" name="size" value="m" class="size-input">
                                    </label>
                                    <label for="size_l">
                                        <a href="#">L</a>
                                        <input type="radio" id="size_l" name="size" value="l" class="size-input">
                                    </label>

                                </div>

                                <p class="price">{{ $product -> price }}</p>

                                <p class="text-center">
                                    <button type="submit" class="btn btn-template-main"><i
                                                class="fa fa-shopping-cart"></i> Add to cart
                                    </button>
                                </p>

                            </form>
                        </div>

                        <div class="row" id="thumbs">
                            <div class="col-4">
                                <a href="{{ asset($product -> image) }}" class="thumb">
                                    <img src="{{ asset($product -> image) }}" alt="" class="img-fluid">
                                </a>
                            </div>
                            <div class="col-4">
                                <a href="{{ asset($product -> image) }}" class="thumb">
                                    <img src="{{ asset($product -> image) }}" alt="" class="img-fluid">
                                </a>
                            </div>
                            <div class="col-4">
                                <a href="{{ asset($product -> image) }}" class="thumb">
                                    <img src="{{ asset($product -> image) }}" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="box" id="details">
                    <p>
                    <h4>Product details</h4>
                    <p>{{ $product -> description }}</p>
                    <button class="btn"><a href="{{ route('question-create', ['id'=>$product->id]) }}">Задать вопрос</a></button>

                </div>
                <div class="box" id="details">
                    <p>
                    <h4>Product Questions</h4>
                        @foreach($questions as $q)
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-title">{{ $q -> title }}</div>
                                    {{ $q -> description }}

                                </div>
                            </div>
                        @endforeach
                </div>

            </div>
            <div class="col-3 mt-2">
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading text-right">
                        <h3 class="panel-title">Categories</h3>
                    </div>

                    <div class="panel-body">
                        <ul class="nav flex-column text-right">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">Active
                                </a>
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link " href="#">Active</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Link</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Link</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                        </ul>


                    </div>
                </div>
            </div>


        </div>
    </div>
    <script>
        document.getElementById('del').addEventListener("click", sure);
        function sure() {
            var button = document.getElementById("del");
            if (button.value != "Вы уверены?") {
                button.classList.add('btn-danger')
                button.value = "Вы уверены?"
            }else {
                button.onclick = location.href="{{ url('product/delete/'.$product->id ) }}";
            }
        }

    </script>
    @endsection('content')