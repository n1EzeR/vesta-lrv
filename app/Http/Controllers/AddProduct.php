<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class AddProduct extends Controller
{
    public function index() {
        return view('product-add');
    }
    public function store(Request $request) {
        $data = $request->all();
        DB::table('product') -> insert(
          ['name' => $data['name'], 'price' => $data['price'],
              'description' => $data['description'], 'created_at' => date('Y-m-d H:i:s'),
              'updated_at' => date('Y-m-d H:i:s'), 'image' => 'img/image.png',
              'subcat_id' => 1
          ]
        );
        return redirect('product/create');
    }
}
