<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeleteProduct extends Controller
{
    public function delete($id) {
        DB::table('product')->where('id','=',$id)->delete();
        $product = DB::table('product')->get();
        return redirect()->route('/', ['product'=> $product]);
    }
}
