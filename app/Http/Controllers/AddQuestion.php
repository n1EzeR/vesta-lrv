<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class AddQuestion extends Controller
{
    public function create($id) {
        $data = [
          'id' => $id
        ];

        return view('product-questions') -> with('id', $id);
    }
    public function store(Request $request, $id) {
        $data = $request->all();
        DB::table('question') -> insert(
            ['title' => $data['title'],
                'description' => $data['description'], 'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'product_id' => $id
            ]
        );
        return redirect('questions');
    }
}
