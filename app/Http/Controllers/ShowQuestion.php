<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ShowQuestion extends Controller
{
    public function index() {
        $question = DB::table('question')-> paginate(5);
        $data = array(
            'question'=>$question
        );
        return view('questions-list', $data);
    }
}
