<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
class ProductController extends Controller
{
    public function index($id) {
        $product = Product::where('id','=',$id) ->first();
        $questions = DB::table('question')->where('product_id','=',$id)->get();
        return view('product-detail', ['product' => $product, 'questions' => $questions]);
    }
}
