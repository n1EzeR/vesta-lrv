<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class EditProduct extends Controller
{
    public function index($id) {
        $product = Product::where('id','=',$id) ->first();
        return view('product-edit', ['product' => $product]);
    }
    public function update(Request $request) {
        $data = $request -> all();
        Product::where('id','=',$data['id'])
            ->update(
                [
                    'name' => $data['name'],
                    'price' => $data['price'],
                    'description' => $data['description']
                ]
            );
        $product = DB::table('product')->get();
        return redirect()->route('/', ['product'=> $product]);
    }
}
