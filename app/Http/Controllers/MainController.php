<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
class MainController extends Controller
{
    public function index() {
        $product = DB::table('product')-> paginate(5);
        $data = array(
            'product'=>$product
        );
        return view('products-list', $data);
    }
    public function sort(Request $request) {
        $type = $request->asc;
        if ($type === "1"){
            $product = DB::table('product')->orderBy('price', 'asc')->paginate(5);
        }
        else {
            $product = DB::table('product')->orderBy('price', 'desc')->paginate(5);
        }
        $data = array(
            'product'=>$product,
            'type' => $type
        );
        return view('products-list', $data);

    }

}
