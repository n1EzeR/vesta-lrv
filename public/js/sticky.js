window.onscroll = function() {myFunction()};

// Get the navbar
var navbar = document.getElementById("myNav");
//var home = document.getElementById("currentHome");
// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("fixed-top");
        navbar.classList.add("bg-white");
    } else {
        navbar.classList.remove("fixed-top");
        navbar.classList.remove("bg-light");
    }
}