<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index') -> name('/');
Route::get('product/detail/{id}','ProductController@index');
Route::get('product/create','AddProduct@index')->name('product-create');
Route::post('product/store','AddProduct@store')->name('product-store');
Route::get('sort','MainController@sort')->name('sort');
Route::get('product/read/{id}','EditProduct@index')->name('product-read');
Route::post('product/update/{id}','EditProduct@update')->name('product-update');
Route::get('product/delete/{id}','DeleteProduct@delete');
Route::get('product/{id}/question-add', ['as'=> 'question-create', 'uses'=>'AddQuestion@create']);
Route::post('product/{id}/question-store', ['as'=> 'question-store', 'uses'=>'AddQuestion@store']);
Route::get('questions','ShowQuestion@index')->name('questions');


